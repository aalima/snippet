FROM golang:1.15

RUN mkdir /app
COPY . /app
WORKDIR /app

RUN go build -o snippetbox ./cmd/web

CMD ["./snippetbox"]

